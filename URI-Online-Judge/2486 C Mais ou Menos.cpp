#include<bits/stdc++.h>
using namespace std;

int main()
{
    int n,r=0,t;
    string s;

    while(cin>>t&&t)
    {

        while(t--)
        {
            scanf("%d", &n);
            cin.ignore();
            getline(cin,s);
            if(s=="suco de laranja") r+=120*n;
            if(s=="goiaba vermelha") r+=70*n;
            if(s=="laranja") r+=50*n;
            if(s=="brocolis") r+=34*n;
            if(s=="morango fresco") r+=85*n;
            if(s=="mamao") r+=85*n;
            if(s=="manga") r+=56*n;
        }
        if(r>130) cout<<"Menos "<<r-130<<" mg\n";
        if(r<110) cout<<"Mais "<<110-r<<" mg\n";
        if(r>=110&&r<=130) cout<<r<<" mg\n";
        r=0;
    }

    return 0;
}
