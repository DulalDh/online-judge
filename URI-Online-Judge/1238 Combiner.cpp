#include <bits/stdc++.h>
using namespace std;
int main()
{
    int T,max;
    string s1,s2;
    cin>>T;
    while(T--)
    {
        cin.ignore();
        cin>>s1;
        cin>>s2;
        max=(s1.size()>s2.size())?s1.size():s2.size();
        for(int i=0; i<max; i++)
        {
            if(i<s1.size())
                cout<<s1[i];
            if(i<s2.size())
                cout<<s2[i];
        }

        cout<<endl;
    }

    return 0;
}
