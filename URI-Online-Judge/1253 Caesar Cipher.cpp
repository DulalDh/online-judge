#include <bits/stdc++.h>
using namespace std;

int main()
{
    int t;
    cin>>t;
    while(t--)
    {
        string s;
        int n;
        cin.ignore();
        getline(cin,s);
        cin>>n;
        for(int i=0;i<s.size();i++)
        {
            if(s[i]-n<'A')
                printf("%c",(s[i]-n)+26);
            else
            printf("%c",s[i]-n);
        }

        cout<<endl;
    }


}
