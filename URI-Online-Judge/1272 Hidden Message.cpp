
#include <bits/stdc++.h>
using namespace std;

int main()
{
    int t;
    string s;
    cin>>t;
    cin.ignore();
    while(t--)
    {
        getline(cin,s);
        if(s[0]!=' '&&s[0]!='.')
            cout<<s[0];
        for(int i=0; i<s.size(); i++)
        {
            if((s[i]==' '||s[i]=='.')&&(s[i+1]>='a'&&s[i+1]<='z'))
            {
                cout<<s[++i];
            }
        }
        cout<<endl;
    }
}
