
#include<bits/stdc++.h>
using namespace std;

int main()
{

    int t;
    string s;
    while(cin>>t)
    {
        cin.ignore();
        while(t--)
        {
            getline(cin,s);

            if(s==".")
                cout<<"a\n";
            if(s=="..")
                cout<<"b\n";
            if(s=="...")
                cout<<"c\n";

            if(s==". .")
                cout<<"d\n";
            if(s==".. ..")
                cout<<"e\n";
            if(s=="... ...")
                cout<<"f\n";

            if(s==". . .")
                cout<<"g\n";
            if(s==".. .. ..")
                cout<<"h\n";
            if(s=="... ... ...")
                cout<<"i\n";

            if(s==". . . .")
                cout<<"j\n";
            if(s==".. .. .. ..")
                cout<<"k\n";
            if(s=="... ... ... ...")
                cout<<"l\n";

            if(s==". . . . .")
                cout<<"m\n";
            if(s==".. .. .. .. ..")
                cout<<"n\n";
            if(s=="... ... ... ... ...")
                cout<<"o\n";

            if(s==". . . . . .")
                cout<<"p\n";
            if(s==".. .. .. .. .. ..")
                cout<<"q\n";
            if(s=="... ... ... ... ... ...")
                cout<<"r\n";

            if(s==". . . . . . .")
                cout<<"s\n";
            if(s==".. .. .. .. .. .. ..")
                cout<<"t\n";
            if(s=="... ... ... ... ... ... ...")
                cout<<"u\n";

            if(s==". . . . . . . .")
                cout<<"v\n";
            if(s==".. .. .. .. .. .. .. ..")
                cout<<"w\n";
            if(s=="... ... ... ... ... ... ... ...")
                cout<<"x\n";

            if(s==". . . . . . . . .")
                cout<<"y\n";
            if(s==".. .. .. .. .. .. .. .. ..")
                cout<<"z\n";

        }
    }
    return 0;
}
