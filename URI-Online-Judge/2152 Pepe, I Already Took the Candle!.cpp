#include<bits/stdc++.h>
using namespace std;

int main()
{
    int n,a,b,c;
    cin>>n;
    while(n--)
    {
        cin>>a>>b>>c;
        if(c==1)
        {
            if(a<10 && b<10)
            cout<<"0"<<a<<":"<<"0"<<b<<" - A porta abriu!\n";
            if(a<10 && b>=10)
            cout<<"0"<<a<<":"<<b<<" - A porta abriu!\n";
            if(a>=10 && b<10)
            cout<<a<<":"<<"0"<<b<<" - A porta abriu!\n";
            if(a>=10 && b>=10)
            cout<<a<<":"<<b<<" - A porta abriu!\n";

        }
        if(c==0)
        {
            if(a<10 && b<10)
            cout<<"0"<<a<<":"<<"0"<<b<<" - A porta fechou!\n";
            if(a<10 && b>=10)
            cout<<"0"<<a<<":"<<b<<" - A porta fechou!\n";
            if(a>=10 && b<10)
            cout<<a<<":"<<"0"<<b<<" - A porta fechou!\n";
            if(a>=10 && b>=10)
            cout<<a<<":"<<b<<" - A porta fechou!\n";

        }
    }
    return 0;
}
